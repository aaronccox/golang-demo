package sum_test

import (
	"testing"

	"bitbucket.org/golang-demo/sum"
)

func TestCalcSum(t *testing.T) {
	source := []int{7, 2, 8, -9, 4, 0, 1}
	expected := 7 + 2 + 8 - 9 + 4 + 0 + 1

	received := sum.CalcSum(source)

	if received != expected {
		t.Fatalf("Expecting %d, not %d", expected, received)
	}
}
