package sum

import "sync"

// SafeAdder is a thread safe adding utility for this demo
type SafeAdder struct {
	value int
	mux   sync.Mutex
}

// Add adds param to the SafeAddr.value
func (c *SafeAdder) Add(param int) {
	c.mux.Lock()
	c.value += param
	c.mux.Unlock()
}

// Value returns the current value of the adder,
func (c *SafeAdder) Value() int {
	c.mux.Lock()
	defer c.mux.Unlock()
	return c.value
}
