package sum

func sum(source []int, safeAdder *SafeAdder, quit chan bool) {
	for _, val := range source {
		safeAdder.Add(val)
	}

	close(quit)
}

// CalcSum returns the sum of the elements found in source.
func CalcSum(source []int) int {

	safeAdder := SafeAdder{}

	quit1 := make(chan bool)
	go sum(source[:len(source)/2], &safeAdder, quit1)

	quit2 := make(chan bool)
	go sum(source[len(source)/2:], &safeAdder, quit2)

	<-quit1
	<-quit2

	return safeAdder.Value()
}
