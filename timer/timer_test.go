package timer

import (
	"fmt"
	"testing"
	"time"
)

func TestRegisterEvent(t *testing.T) {
	timer := newInstance()

	nowDate := time.Now()
	now := time.Now().Format(timeFormat)
	scheduled := nowDate.Add(2 * time.Second).Format(timeFormat)
	expected := 1

	fmt.Printf("Now:           %s\n", now)
	fmt.Printf("Secheduled:    %s\n", scheduled)

	calledBackCount := 0
	quit := make(chan bool)

	timer.RegisterEvent(scheduled, func(time.Time) {
		fmt.Printf("Called Back:   %s\n", scheduled)
		calledBackCount++
		close(quit)
	})

	// <-quit

	timeout := nowDate.Add(3 * time.Second)
	endless := true
	for endless && time.Now().Before(timeout) {
		select {
		case <-quit:
			endless = false
			break

		default:
			time.Sleep(50 * time.Millisecond)
		}
	}

	if expected != calledBackCount {
		fmt.Printf("Test Fails:    %s\n", time.Now().Format(timeFormat))
		t.Fatalf("Expecting %d call backs; but only received %d", expected, calledBackCount)
	}

	fmt.Printf("Test Succeeds: %s\n", time.Now().Format(timeFormat))
	timer.Close()
}

func TestForwardCalcDuration(t *testing.T) {
	D := calcDuration("08:30:01", "08:35:01")

	expect := 5 * 60 * 1000 * time.Millisecond
	if expect != D {
		t.Fatalf("Expecting %v, got %v", expect, D)
	}
}

func TestBackwardCalcDuration(t *testing.T) {
	D := calcDuration("08:30:01", "08:25:01")

	expect, _ := time.ParseDuration("23h55m0s")
	if expect != D {
		t.Fatalf("Expecting %v, got %v", expect, D)
	}
}
