package timer

import (
	"sync"
	"time"
)

const timeFormat = "15:04:05"

type Timer interface {
	RegisterEvent(string, func(time.Time))
	RegisterRecurringEvent(string, string, func(time.Time))
	Close()
}

type timer struct {
	channel       chan timedEvent
	quit          chan bool
	closed        bool
	eventsInQueue int
	mux           sync.Mutex
}

type timedEvent struct {
	Duration  time.Duration   // time between now and target event
	Recurring time.Duration   // optional duration between recurring events
	FU        func(time.Time) // function to call when event is triggered
}

var instance *timer
var once sync.Once

// GetTimer returns a global instance of a Timer.
func GetTimer() Timer {
	once.Do(func() {
		instance = newInstance()
	})

	return instance
}

func newInstance() *timer {
	localTimer := &timer{
		channel: make(chan timedEvent),
		quit:    make(chan bool),
	}
	go localTimer.listener()
	return localTimer
}

func (t *timer) Close() {
	t.closed = true
	close(t.channel)
	<-t.quit
}

func (t *timer) RegisterRecurringEvent(timeStamp string, incrementDuration string, fu func(time.Time)) {
	now := time.Now().Format(timeFormat)
	D := calcDuration(now, timeStamp)
	I, err := time.ParseDuration(incrementDuration)
	if err != nil {
		panic(err)
	}

	te := timedEvent{
		Duration:  D,
		Recurring: I,
		FU:        fu,
	}

	t.addToQueue(1)
	t.channel <- te
}

func (t *timer) RegisterEvent(timeStamp string, fu func(time.Time)) {
	now := time.Now().Format(timeFormat)
	D := calcDuration(now, timeStamp)
	te := timedEvent{
		Duration: D,
		FU:       fu,
	}

	t.addToQueue(1)
	t.channel <- te
}

func (t *timer) addToQueue(increment int) {
	t.mux.Lock()
	t.eventsInQueue += increment
	t.mux.Unlock()
}

func (t *timer) listener() {
	for te := range t.channel {
		go t.sleepAndEmit(te)
	}

	for t.eventsInQueue > 0 {
		time.Sleep(5 * time.Second)
	}

	t.quit <- true
}

func (t *timer) sleepAndEmit(te timedEvent) {
	time.Sleep(te.Duration)
	go te.FU(time.Now())

	if te.Recurring != 0 && !t.closed {
		re := timedEvent{
			Duration:  te.Recurring,
			Recurring: te.Recurring,
			FU:        te.FU,
		}
		t.channel <- re
	} else {
		t.addToQueue(-1)
	}
}

func calcDuration(now string, target string) time.Duration {
	from, _ := time.Parse(timeFormat, now)
	to, _ := time.Parse(timeFormat, target)
	if to.After(from) {
		return to.Sub(from)
	}
	to = to.Add(24 * time.Hour)
	return to.Sub(from)
}
