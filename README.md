# golang-demo
A playground for example code segments for brown bags.

### Branches
Branch Name | Purpose
--- | ---
master         | Latest with up to date working set.
sum-no-channel | Simple sum test with no channels.
sum-channel    | Introduce channels into sum.
sub-channel    | Alternate implementation using mutex.
cron           | Some cron like stuff.


### Blog Flow

#### Introduce Calc Sum

https://tour.golang.org/concurrency/2

Show sum_test.go behavior w/o channels.

branch: sum-no-channel

#### Put channels into sum
branch: sum-channel

Show Deadlock / Talk about default channel blocking behavior


#### Re-work sum using mutex
branch: sub-mutex  (spelled wrong)

https://tour.golang.org/concurrency/9

Walk through mutex solution

Show race condition (failed test) by commenting out the quit listeners


#### Look at cron like stuff
branch: cron

Look through tests, showing channel usage within the test itself.

Invistigate two examples of having the test block to wait, explore long running test.

If time permits; walk through the timer code.


